// importe le module Express
const express = require("express");
require("dotenv").config();
const connection = require("./conf/db");
// import de la bibiothèque cors
const cors = require("cors");


const app = express();

// Configure l'application pour parser les requêtes avec des corps au format JSON.
app.use(express.json());

// Configure l'application pour parser les requêtes avec des corps encodés en URL (utile pour les formulaires web).
app.use(express.urlencoded({extended: true,}));

// utilisation du port 3000 pour accéder au serveur
const port = 3000;

// ajout d'une configuration pour gérer les CORS 
app.use(
  cors({
    origin: "http://localhost:8080",
  })
);



/*Routes pour le CRUD */

//route GET sur l'URL racine ('/')
app.get("/", (req, res) => {
  res.send("Hello World!");
});

//route GET pour récupérer toutes les voitures de la base de données
app.get('/cars', (req, res) => {
  // Exécution d'une requête SQL pour sélectionner toutes les voitures
  connection.query('SELECT * FROM car', (err, results) => {
    // Gestion d'erreurs éventuelles lors de l'exécution de la requête
    if (err) {
      res.status(500).send('Erreur lors de la récupération des voitures');
    } else {
      // Envoi des résultats sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});

// Route GET pour récupérer une voiture spécifique par son ID en utilisant une requête préparée pour éviter les injections SQL
app.get('/cars/:id', (req, res) => {
  const id = req.params.id;
  // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
  connection.execute('SELECT * FROM car WHERE car_id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la récupération de la voiture');
    } else {
      // Envoi du résultat sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});

// Route POST pour ajouter une nouvelle voiture dans la base de données
app.post('/cars', (req, res) => {
  const { brand, model } = req.body;
  // Exécution d'une requête SQL pour insérer une voiture
  connection.execute(
    "INSERT INTO car (brand, model) VALUES (?, ?)",
    [brand, model],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de l'ajout de la voiture");
      } else {
        // Confirmation de l'ajout dela voiture avec l'ID de la voiture insérée
        res.status(201).send(`Voiture ajoutée avec l'ID ${results.insertId}`);
      }
    }
  );
});

// Route PUT pour mettre à jour une voiture existante par son ID
app.put('/cars/:id', (req, res) => {
  const { brand, model } = req.body;
  const id = req.params.id;
  // Exécution d'une requête SQL pour mettre à jour les informations de la voiture spécifiée
  connection.execute(
    "UPDATE car SET brand = ?, model = ? WHERE car_id = ?",
    [brand, model, id],
    (err, result) => {
      if (err) {
        res.status(500).send("Erreur lors de la mise à jour de la voiture");
      } else {
        // Confirmation de la mise à jour de la voiture
        res.send(`Voiture mise à jour.`);
      }
    }
  );
});

// Route DELETE pour supprimer une voiture par son ID
app.delete('/cars/:id', (req, res) => {
    //recherche avec le paramètre de l'url :id
  const id = req.params.id;
  // Exécution d'une requête SQL pour supprimer le livre spécifié
  connection.execute('DELETE FROM car WHERE car_id = ?', [id], (err, results) => {
    if (err) {
      res.status(500).send('Erreur lors de la suppression de la voiture');
    } else {
      // Confirmation de la suppression du livre
      res.send(`Voiture supprimée.`);
    }
  });
});


// Route GET pour récupérer les voitures d'une marque
app.get('/brand', (req, res) => {
  //recherche avec query pour récupérer le paramètre de requête et non le paramètre de l'url
  const brand = req.query.brand;
  // Utilisation d'une requête préparée avec un paramètre pour sécuriser l'accès aux données
  connection.execute(
    "SELECT * FROM car WHERE brand = ?",
    [brand],
    (err, results) => {
      if (err) {
        res.status(500).send("Erreur lors de la récupération de la marque");
      } else {
        // Envoi du résultat sous forme de JSON si la requête est réussie
        res.json(results);
      }
    }
  );
});


// Démarre le serveur Express et écoute sur le port spécifié
app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});

